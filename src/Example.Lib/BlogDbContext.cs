using Example.Lib.Entities;
using Microsoft.EntityFrameworkCore;

namespace Example.Lib;

public class BlogDbContext : DbContext
{
    public BlogDbContext(DbContextOptions<BlogDbContext> options)
        : base(options)
    {
    }
    
    public DbSet<User> Users { get; set; }
    public DbSet<Blog> Blogs { get; set; }
}