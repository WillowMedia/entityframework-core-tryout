using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Example.Lib.Entities;

[Index(nameof(Title), nameof(CreatedOn))]
public class Blog
{
    // Optionally use [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Key]
    public int? Id { get; set; }
    
    [Required]
    public string? Title { get; set; }
    
    public string? Content { get; set; }
    
    public DateTime? CreatedOn { get; set; }
    
    [Required]
    public User? User { get; set; }
}