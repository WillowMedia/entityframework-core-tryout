using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Example.Lib.Entities;

[Index(nameof(Email))]
public class User
{
    // Optionally use [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Key]
    public int? Id { get; set; }

    [Required]
    public string? Name { get; set; }

    [EmailAddress]
    public string? Email { get; set; }
}