
## using the entityframework dotnet tools

Create the migration:

```bash
dotnet ef migrations add Database_v0 
```

Remove the migrations:

```bash
dotnet ef migrations remove 
```
