using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Example.Lib;

public class DesignTimeContextFactory : IDesignTimeDbContextFactory<BlogDbContext>
{
    public BlogDbContext CreateDbContext(string[] args)
    {
        var connectionString = "Data Source=mytestdatabase.db;mode=memory;cache=shared";
        var options = new DbContextOptionsBuilder<BlogDbContext>()
            .UseSqlite(connectionString)
            .Options;

        return new BlogDbContext(options);
    }
}