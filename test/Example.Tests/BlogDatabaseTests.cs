using Example.Lib;
using Example.Lib.Entities;

namespace Example.Tests;

public class BlogDatabaseTests
{
    /// <summary>
    /// Lets prove the users are referenced to the blog items on save and load. Keep in mind that in this test,
    /// this might be done in memory, and not really in the database. We should add logging to be sure
    /// </summary>
    [Fact]
    public void SaveBlogsWithUserTest()
    {
        // Use in sqlite memory database for testing
        var contextFactory = new MemoryDatabaseContextFactory<BlogDbContext>();
        
        // var connectionString = "Data Source=mytestdatabase.db;mode=memory;cache=shared";
        // var options = new DbContextOptionsBuilder<BlogDbContext>()
        //     .UseSqlite(connectionString)
        //     .Options;
        
        using (var context = contextFactory.Create()) // new BlogDbContext(options))
        {
            // Create 2 users
            context.Users.Add(new User { Name = "Peter", Email = "peter@my-fake-domain.com "});
            context.Users.Add(new User { Name = "nat", Email = "nat@my-fake-domain.com "});
            context.SaveChanges();

            // Get user
            var user = context.Users.FirstOrDefault(u => u.Name == "Peter");
            Assert.NotNull(user);

            // Create blogs
            var blog1 = new Blog() { Title = "Blog 1", User = user, CreatedOn = DateTime.Now.AddDays(-1) };
            context.Blogs.Add(blog1);
            context.SaveChanges();
            Assert.NotNull(blog1.Id);
            // When using GUID, test if the guid isn't 0000-00...
            // Assert.NotEqual(new Guid(), blog1.Id);
            
            var blog2 = new Blog() { Title = "Blog 2", User = user, CreatedOn = DateTime.Now };
            context.Blogs.Add(blog2);
            context.SaveChanges();
            Assert.NotNull(blog2.Id);
            // When using GUID, test if the guid isn't 0000-00...
            // Assert.NotEqual(new Guid(), blog2.Id);

            // Get all from DB
            var blogsFromDb = context.Blogs.ToList();
            Assert.Equal(2, blogsFromDb.Count);
            
            // Get single
            var blog = context.Blogs.FirstOrDefault(b => b.Id == blog2.Id);
            Assert.NotNull(blog);
            Assert.Equal(user, blog.User);
        }
    }
}