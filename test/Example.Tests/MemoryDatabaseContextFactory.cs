using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace Example.Tests;

public class MemoryDatabaseContextFactory<T> where T: DbContext
{
    protected readonly DbContextOptions _options;

    public MemoryDatabaseContextFactory()
    {
        var builder = new DbContextOptionsBuilder<T>();
        var connection = new SqliteConnection("DataSource=:memory:");

        connection.Open();
        builder.UseSqlite(connection);

        using (var ctx = (T)Activator.CreateInstance(typeof(T), new[] { builder.Options }))
        {
            ctx.Database.EnsureCreated();
        }

        _options = builder.Options;
    }

    public T Create() =>  (T)Activator.CreateInstance(typeof(T), new[] { _options });
}